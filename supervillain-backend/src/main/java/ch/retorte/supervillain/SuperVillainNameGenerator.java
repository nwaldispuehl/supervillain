package ch.retorte.supervillain;

import java.util.Random;

import static java.lang.String.format;
import static java.util.Arrays.asList;

/**
 * Name generator for super villains.
 */
public class SuperVillainNameGenerator {

  //---- Statics

  private static final String[] PREFIX = {"Sir", "Dr.", "The", "Mr.", "", "", "", "", "", ""};
  private static final String[] FIRST_PART = {"Bat", "Atom", "Aqua", "Iron", "Ghost", "Rocket", "Mutant", "Savage", "Joker", "Ant", "Incredible", "Shadow", "Blue", "Black", "Green", "Red", "Nuclear", "Mega", "Fluffy", "Strong", "Angry", "","Steel", "Annoyed", "Displeased", "Enraged", "Swollen", "Smoking", "Dark", "Evil", "Poison", "Freeze", "", "", ""};
  private static final String[] SECOND_PART = {"Man", "Canary", "Turtle", "Panther", "Kid", "Lantern", "Woman", "Death", "Fist", "Leg", "Arm", "Ripper", "Thing", "Voldemort", "Tron", "Thief", "Bird", "Gun", "Knife", "Preacher", "Bot", "Rifle", "Bomb", ""};


  //---- Fields

  private final Random random = new Random();


  //---- Methods

  /**
   * Generates a random super villain name.
   *
   * @return a randomly composed string with a villain name.
   */
  public String generate() {
    return format("%s %s %s", pick(PREFIX), pick(FIRST_PART), pick(SECOND_PART)).trim();
  }

  private <T> T pick(T[] t) {
    return asList(t).get(random.nextInt(t.length));
  }

}

package ch.retorte.supervillain;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerRequest;

/**
 * Entry point for the Supervillain Name Generator verticle.
 */
public class Start extends AbstractVerticle {

  //---- Statics

  private static final int PORT = 8080;
  private static final String VILLAIN_PATH = "villain";
  private static final String KILL_PATH = "kill";


  //---- Methods

  @Override
  public void start() {
    vertx.createHttpServer().requestHandler(this::handle).listen(PORT);
    log("Supervillain Name Generator started.");
  }

  private void handle(HttpServerRequest request) {
    if (villainPath(request)) {
      String superVillainName = new SuperVillainNameGenerator().generate();
      log("Received 'villain' command. Sending villain name: " + superVillainName);
      request.response().end(createJsonResponseWith(superVillainName));
    }

    else if (killPath(request)) {
      log("Received 'kill' command. Terminating...");
      request.response().end();

      // We terminate the application.
      vertx.close();
    }

    else {
      log("Received unknown command '" + request.path() + "'.");
      request.response().setStatusCode(404).end("unknown resource");
    }
  }

  private boolean villainPath(HttpServerRequest request) {
    return pathEndsWith(request, VILLAIN_PATH);
  }

  private String createJsonResponseWith(String superVillainName) {
    return String.format("{ \"name\" : \"%s\" }", superVillainName);
  }

  private boolean killPath(HttpServerRequest request) {
    return pathEndsWith(request, KILL_PATH);
  }

  private boolean pathEndsWith(HttpServerRequest request, String s) {
    return request.path().endsWith(s);
  }

  private void log(String message) {
    System.out.println(message);
  }

}

# Supervillain Name Generator Backend

Build with:

    $ ./gradlew shadowjar

Create image with:

    $ docker build -t supervillain-backend .

Create image tagged for registry with:

    $ docker build -t registry.gitlab.com/nwaldispuehl/supervillain/backend .

Push image

    $ docker push registry.gitlab.com/nwaldispuehl/supervillain/backend

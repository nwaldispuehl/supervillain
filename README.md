# Supervillain Name Generator 

A Docker demo application with two nodes:

- Supervillain Frontend: Shows a GUI
- Supervillain Backend: Generates Supervillain names

## How to use:

First, build and dockerize both subprojects.
Then, start the project with docker-compose:

    $ cd compose.local
    $ docker-compose up

## How to use with pre-built images:

    $ cd compose.registry
    $ docker-compose up

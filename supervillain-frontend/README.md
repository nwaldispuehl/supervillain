# Supervillain Name Generator Frontend

Build docker image with:

    $ docker build -t supervillain-frontend .

Build image tagged for registry with:

    $ docker build -t registry.gitlab.com/nwaldispuehl/supervillain/frontend .

Push image

    $ docker push registry.gitlab.com/nwaldispuehl/supervillain/frontend
